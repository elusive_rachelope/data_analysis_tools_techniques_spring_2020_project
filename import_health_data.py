import pandas as pd
import numpy as np
import data_parse
import matplotlib.pyplot as plt
from itertools import combinations

drive_sheet_name = 'Ranked Measure Data'
drive_cols = ['County', '% Drive Alone', '% Long Commute - Drives Alone', '% Vaccinated', '% Vaccinated (Black)', '% Vaccinated (Hispanic)', '% Vaccinated (White)',
              'Graduation Rate', '% Some College', '% Children in Poverty']

sleep_sheet_name = 'Additional Measure Data'
sleep_cols = ['County', '% Insufficient Sleep', '# Motor Vehicle Deaths', 'MV Mortality Rate']

drive_df = data_parse.read_data_all_years(drive_sheet_name, drive_cols)
print(drive_df.tail())
print(drive_df.head())
sleep_df = data_parse.read_data_all_years(sleep_sheet_name, sleep_cols)

df = drive_df.merge(sleep_df, on=['County', 'Year'], how='inner')

# Get all permutations of length 2
# and length 2
comb = combinations(df.drop(columns=['County']).columns.values, 2)

# Print the obtained permutations
ind = 0
for i in list(comb):
    ind += 1
    print(str(ind) + ': x=' + str(i[0]) + ' ' +str(len(i[0])) + ' y=' + str(i[1]) + ' ' + str(len(i[1])))

    sctrplt = df.plot.scatter(x=i[0], y=i[1])
    fig = sctrplt.get_figure()
    plt.savefig(r'C:\Users\Rachel\Documents\School\Spring 2020\Data Analytics Tools and Techniques\Project\plots\plot' + str(ind) +'.png')



