import pandas as pd
from functools import reduce
import os


def read_data_all_years(sheet_name, cols):
    dfs = list()
    for filename in os.scandir('.\\data'):
        df = read_sheet(filename.path, sheet_name, cols)
        dfs.append(df)
    df_final = pd.concat(dfs)
    return df_final

def read_sheet(file_path, sheet_name, cols):
    data = pd.read_excel(file_path, header=1, sheet_name=sheet_name)
    df = pd.DataFrame(data, columns=cols)
    df['Year']=str(os.path.basename(file_path))[0:4]
    return df
